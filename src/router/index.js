import MainSection from "../components/MainSection";
import AppBookMarks from "../components/AppBookMarks";
import DetailPage from "../components/DetailPage"
export const routes = [
  { path: "/", name: "MainSection", component: MainSection },
  { path: "/bookmarks", name: "AppBookMarks", component: AppBookMarks },
  {path:"/details/:id", name:"detailPage",component: DetailPage}
];
